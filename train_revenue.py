import numpy as np
import pandas as pd
import pickle
from sklearn.preprocessing import LabelEncoder
from sklearn.ensemble import RandomForestRegressor

def remove_outliers(users):
	return users[users['revenue'] < 10]


def transform_data(users):
	country_encoder = LabelEncoder()
	users['country'] = country_encoder.fit_transform(users['country'])
	pickle.dump(country_encoder, open('country_encoder.pkl', 'wb'))

	source_encoder = LabelEncoder()
	users['source'] = users['source'].fillna('NULL')
	users['source'] = source_encoder.fit_transform(users['source'])
	pickle.dump(source_encoder, open('source_encoder.pkl', 'wb'))	

	platform_encoder = LabelEncoder()
	users['platform'] = users['platform'].apply(lambda row: 'iOS' if row == 'ios' else row)
	users['platform'] = users['platform'].apply(lambda row: 'Android' if row == 'android' else row)
	users['platform'] = platform_encoder.fit_transform(users['platform'])
	pickle.dump(platform_encoder, open('platform_encoder.pkl', 'wb'))

	device_family_encoder = LabelEncoder()
	users['device_family'] = device_family_encoder.fit_transform(users['device_family'])
	pickle.dump(device_family_encoder, open('device_family_encoder.pkl', 'wb'))

	# tiro los ids. Creo que no me sirven	
	users = users.drop('user_id', axis=1)

	y = users['revenue']
	X = users.drop('revenue', axis=1)
	return X, y

# levanto en memoria el dataset
users = pd.read_csv('dataset_v2.csv')
users = remove_outliers(users)

X, y = transform_data(users) # no separo en train y test porque esto es el ambiente productivo USO TODOS LOS DATOS

rfr_regressor = RandomForestRegressor()
rfr_regressor.fit(X,y)

pickle.dump(rfr_regressor, open('predictor.pkl', 'wb'))

print("predictor.pkl was dumped to disk")
