# README #

This project is a challenge corresponding to interviews for a job position at Etermax

### What is this repository for? ###

* This is a project that generates a model to predict users revenue.
* Then, this model is deploy to a microservice:
	* **receives:** user atributes (country, source, platform, device_family)
	* **returns:** the predicted revenue value
* version 1.0.0

### Contents ###

* Dataset (dataset_v2.csv)
* Notebook file with an exploratory code (MLE-challenge.ipynb)
* Script that trains the model (train_revenue.py)
* Microservice code (revenue_ms.py)
* Predictor object saved on disk (predictor.pkl)

### Who do I talk to? ###

* Leandro Nahabedian: leanahabedian@gmail.com