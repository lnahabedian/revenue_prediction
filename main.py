from fastapi import FastAPI
from pydantic import BaseModel
import pickle
import numpy as np
import pandas as pd

app = FastAPI()

class User(BaseModel):
	country: int
	source: int
	platform: int
	device_family: int
	event_1: int
	event_2: int

@app.post('/predict')
async def predict_diabetes(user: User):
	data = user.dict()
	loaded_model = pickle.load(open('predictor.pkl', 'rb'))
	data_in = [[data['country'], data['source'], data['platform'], data['device_family'], data['event_1'], data['event_2']]]
	prediction = loaded_model.predict(data_in)

	return {
		'prediction': prediction[0],
	}