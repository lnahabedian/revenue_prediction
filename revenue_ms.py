from flask import Flask, request
import pickle
import ast

app = Flask(__name__)
users_seen = {}


@app.route('/')
def hello():
    user_agent = request.headers.get('User-Agent')
    return 'Hello! I see you are using %s' % user_agent

@app.route('/predict', methods=['POST'])
async def predict_user():
	byte_str = request.stream.read()
	new_str = byte_str.decode('utf-8')
	data = ast.literal_eval(new_str)
	data_in = [[data['country'], data['source'], data['platform'], data['device_family'], data['event_1'], data['event_2']]]
	prediction = loaded_model.predict(data_in)

	#if event_1 > 80 and event_2 > 10:


	return {
		'prediction': prediction[0],
	}

if __name__ == '__main__':
	loaded_model = pickle.load(open('predictor.pkl', 'rb'))
	app.run(host='127.0.0.1', port=5000)